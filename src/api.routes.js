export const apiRoutes = {
    register: route('/api/user/register'),
    login: route('/api/user/login'),
    pointOfInterestList: route('/api/poi/list'),
    pointOfInterestDetail: (id) => route(`/api/poi/${id}`),
    // ...
}

/**
 * Prepends API endpoint to route
 * @param {string} path 
 * @returns {string}
 */
function route(path) {
    return `${process.env.REACT_APP_API_ENDPOINT}${path}`
}