import React, { Component } from "react";
import { Container, Button } from "reactstrap";
import { Link } from "react-router-dom";
import { Parallax } from "react-parallax";
import { HashLink } from "react-router-hash-link";
import { apiRoutes } from "../api.routes";
import { ApiService } from "../api.service";

class General extends Component {
  state = {};

  render() {
    console.log("start");

    var data = "id=1&email=hz&password=1234&user_name=dima";
    const payload = {
      id: 2,
      email: "di@gmail",
      password: 1234,
      user_name: "Dima"
    }

    ApiService
      .post(apiRoutes.register, payload)
      .then(data => console.log(data))

    console.log("stop");

    return (
      <React.Fragment>
        {/* -----basic config-----*/}
        <Parallax
          blur={1}
          bgImage={require("../components/img/paral1.jpg")}
          bgImageAlt="nature"
          bgImageStyle={{ opacity: "0.8" }}
          strength={500}
        >
          <Container className="text-center centered ">
            <hr />
            <h3 className="display-4 text-black">
              General information about our services
            </h3>
            <hr />
            <p className="lead text-justify text-black">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt
              culpa laboriosam labore temporibus sint ipsa sit corporis
              perspiciatis officiis inventore, unde. Necessitatibus impedit
              fuga, quasi error numquam atque cumque dicta vero vitae iusto eos
              dignissimos ducimus dolor inventore repudiandae perferendis minus.
              Architecto illum consequatur necessitatibus, dolores, id commodi
              ipsam asperiores culpa maxime alias reprehenderit! Vel sunt animi
              magnam illum quasi, dolore? Amet, veritatis quasi, praesentium
              nihil perferendis vero, est nisi nobis, fugit numquam modi atque
              voluptatibus harum vitae qui a! Ducimus culpa sunt dolor.
              Reprehenderit exercitationem tenetur fugit ab accusamus commodi,
              deleniti, error cumque. Exercitationem, vitae cumque quasi
              deserunt, maiores incidunt aut, quisquam saepe harum error fuga
            </p>

            <Button
              className="mr-3"
              color="primary"
              tag={HashLink}
              smooth
              to={"/#attracts"}
            >
              Attractions
            </Button>

            <Button
              className="mr-3"
              color="primary"
              tag={HashLink}
              smooth
              to={"/#travel"}
            >
              Make a Route
            </Button>

            <Button
              className="mr-3"
              color="primary"
              tag={HashLink}
              smooth
              to={"/#about"}
            >
              About Us
            </Button>
          </Container>

          <div style={{ height: "800px" }} />
        </Parallax>

        <Container className="text-center">
          <h3 className="display-4" id="attracts">
            View List Of Attractions
          </h3>
        </Container>

        <Parallax
          blur={5}
          bgImage={require("../components/img/paral2.jpg")}
          bgImageAlt="stefan"
          bgImageStyle={{ opacity: "0.8" }}
          strength={1000}
        >
          <Container className="text-center centered">
            <hr />
            <h3 className="display-4 text-black">
              General information about our services
            </h3>

            <p className="lead text-justify text-black">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt
              culpa laboriosam labore temporibus sint ipsa sit corporis
              perspiciatis officiis inventore, unde. Necessitatibus impedit
              fuga, quasi error numquam atque cumque dicta vero vitae iusto eos
              dignissimos ducimus dolor inventore repudiandae perferendis minus.
              Architecto illum consequatur necessitatibus, dolores, id commodi
              ipsam asperiores culpa maxime alias reprehenderit! Vel sunt animi
              magnam illum quasi, dolore? Amet, veritatis quasi, praesentium
              nihil perferendis vero, est nisi nobis, fugit numquam modi atque
              voluptatibus harum vitae qui a! Ducimus culpa sunt dolor.
              Reprehenderit exercitationem tenetur fugit ab accusamus commodi,
              deleniti, error cumque. Exercitationem, vitae cumque quasi
              deserunt, maiores incidunt aut, quisquam saepe harum error fuga
            </p>

            <Button color="primary" tag={Link} to={"/attractions"}>
              Go to Attractions!
            </Button>

            <hr />
          </Container>
          <div style={{ height: "800px" }} />
        </Parallax>

        <Container className="text-center ">
          <h3 className="display-4" id="travel">
            Make a Route
          </h3>
        </Container>

        <Parallax
          blur={5}
          bgImage={require("../components/img/paral3.jpg")}
          bgImageAlt="stefan"
          bgImageStyle={{ opacity: "0.7" }}
          strength={1000}
        >
          <Container className="text-center centered">
            <hr />
            <h3 className="display-4 text-black">
              General information about our services
            </h3>

            <p className=" lead text-justify text-black">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt
              culpa laboriosam labore temporibus sint ipsa sit corporis
              perspiciatis officiis inventore, unde. Necessitatibus impedit
              fuga, quasi error numquam atque cumque dicta vero vitae iusto eos
              dignissimos ducimus dolor inventore repudiandae perferendis minus.
              Architecto illum consequatur necessitatibus, dolores, id commodi
              ipsam asperiores culpa maxime alias reprehenderit! Vel sunt animi
              magnam illum quasi, dolore? Amet, veritatis quasi, praesentium
              nihil perferendis vero, est nisi nobis, fugit numquam modi atque
              voluptatibus harum vitae qui a! Ducimus culpa sunt dolor.
              Reprehenderit exercitationem tenetur fugit ab accusamus commodi,
              deleniti, error cumque. Exercitationem, vitae cumque quasi
              deserunt, maiores incidunt aut, quisquam saepe harum error fuga
            </p>

            <Button color="primary" tag={Link} to={"/travel"}>
              Make your route!
            </Button>
            <hr />
          </Container>
          <div style={{ height: "800px" }} />
        </Parallax>

        <Container className="text-center">
          <h3 className="display-4" id="about">
            About Us
          </h3>
        </Container>

        <Parallax
          blur={1}
          bgImage={require("../components/img/paral4.jpg")}
          bgImageAlt="stefan"
          bgImageStyle={{ opacity: "0.6" }}
          strength={1000}
        >
          <Container className="text-center centered">
            <hr />
            <h3 className="display-4 text-black">
              General information about our services
            </h3>

            <p className=" lead text-justify text-black">
              Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt
              culpa laboriosam labore temporibus sint ipsa sit corporis
              perspiciatis officiis inventore, unde. Necessitatibus impedit
              fuga, quasi error numquam atque cumque dicta vero vitae iusto eos
              dignissimos ducimus dolor inventore repudiandae perferendis minus.
              Architecto illum consequatur necessitatibus, dolores, id commodi
              ipsam asperiores culpa maxime alias reprehenderit! Vel sunt animi
              magnam illum quasi, dolore? Amet, veritatis quasi, praesentium
              nihil perferendis vero, est nisi nobis, fugit numquam modi atque
              voluptatibus harum vitae qui a! Ducimus culpa sunt dolor.
              Reprehenderit exercitationem tenetur fugit ab accusamus commodi,
              deleniti, error cumque. Exercitationem, vitae cumque quasi
              deserunt, maiores incidunt aut, quisquam saepe harum error fuga
            </p>

            <Button color="primary" tag={Link} to={"/about"}>
              About Us ABCDEF
            </Button>

            <hr />
          </Container>
          <div style={{ height: "800px" }} />
        </Parallax>
      </React.Fragment>
    );
  }
}

export default General;
