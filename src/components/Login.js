import React, { Component } from "react";
import { Container, Button, Form, FormGroup, Input } from "reactstrap";
import { Parallax } from "react-parallax";
import { Link } from "react-router-dom";
// import { PostData } from "../services/PostData";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "",
      password: ""
    };
    this.login = this.login.bind(this);
    this.onChange = this.onChange.bind(this);
  }

  login() {
    console.log("lalal");
    // PostData("login", this.state).then;
    // ((result) => {
    //   let responseJSON = result;
    //   console.log(responseJSON);
    // });
  }

  onChange(e) {
    this.setState({ [e.target.name]: e.target.value });
    console.log(this.state);
  }

  render() {
    return (
      <React.Fragment>
        <Parallax
          bgImage={require("../components/img/paral1.jpg")}
          bgImageAlt="nature"
          bgImageStyle={{ opacity: "0.9" }}
          strength={500}
        >
          <Container className="text-center centered">
            <section className="row justify-content-center align-items-center mt-5">
              <section className="col-10 col-xl-4 col-lg-6 col-sm-8">
                <Form className="form-container log">
                  <h3>Log In</h3>
                  <FormGroup className="mt-4">
                    <Input
                      type="email"
                      name="email"
                      id="exampleEmail"
                      placeholder="enter your email..."
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="password"
                      name="password"
                      id="examplePassword"
                      placeholder="enter your password..."
                    />
                  </FormGroup>

                  <Button
                    color="primary"
                    type="submit"
                    onClick={this.login}
                    tag={Link}
                    to={"/"}
                  >
                    Login
                  </Button>
                  <br />
                  <small>
                    Don't have an account?{" "}
                    <Link className="login_link" to={"/signup"}>
                      <strong>Create one!</strong>
                    </Link>
                  </small>
                </Form>
              </section>
            </section>
          </Container>

          <div style={{ height: "800px" }} />
        </Parallax>
      </React.Fragment>
    );
  }
}

export default Login;
