import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Card, CardBody, CardImg, Button } from "reactstrap";

class PointOfInterest extends Component {
  render() {
    const { postDetail } = this.props;
    return (
      <Card className="mt-3">
        <CardImg top className="wh" src={postDetail.image} />
        <CardBody>
          <h5 className="card-title">{postDetail.name}</h5>
          <p className="card-text">
            The cost of the visit is {postDetail.cost} MDL
          </p>
          <Button
            color="primary"
            tag={Link}
            to={`/attractions/${postDetail.id}`}
          >
            View Details
          </Button>
        </CardBody>
      </Card>
    );
  }
}

export default PointOfInterest;

// src={require("../components/img/abc.jpg")}
