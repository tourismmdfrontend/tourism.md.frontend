import React, { Component } from "react";
import { Container } from "reactstrap";

class About extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Container className="text-justify mt-3">
          <hr />
          <h3 className="text-center mt-3 display-4">About Us</h3>
          <hr />
          <p className="lead">
            Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nesciunt
            culpa laboriosam labore temporibus sint ipsa sit corporis
            perspiciatis officiis inventore, unde. Necessitatibus impedit fuga,
            quasi error numquam atque cumque dicta vero vitae iusto eos
            dignissimos ducimus dolor inventore repudiandae perferendis minus.
            Architecto illum consequatur necessitatibus, dolores, id commodi
            ipsam asperiores culpa maxime alias reprehenderit! Vel sunt animi
            magnam illum quasi, dolore? Amet, veritatis quasi, praesentium nihil
            perferendis vero, est nisi nobis, fugit numquam modi atque
            voluptatibus harum vitae qui a! Ducimus culpa sunt dolor.
            Reprehenderit exercitationem tenetur fugit ab accusamus commodi,
            deleniti, error cumque. Exercitationem, vitae cumque quasi deserunt,
            maiores incidunt aut, quisquam saepe harum error fuga ea! Minima
            quasi in, voluptatum molestiae odio explicabo sequi expedita
            consectetur tempore quaerat aperiam tenetur sed rerum assumenda ut
            ducimus corporis soluta perferendis magni dicta voluptate,
            repudiandae eos eligendi. Sapiente laudantium at architecto incidunt
            adipisci magnam doloremque ullam rerum eos libero tempore eveniet
            voluptatum alias placeat cumque quis consequatur soluta, harum, id
            atque? Non soluta reiciendis beatae, quia incidunt mollitia.
            Deserunt laboriosam necessitatibus minima accusamus natus vero
            tempora nihil sequi libero. In accusamus iste itaque sequi iusto,
            molestias quo vel maxime pariatur, nisi nesciunt quidem reiciendis
            corporis, quibusdam? Animi atque eaque quae voluptatem. Lorem ipsum
            dolor sit amet, consectetur adipisicing elit. Nesciunt culpa
            laboriosam labore temporibus sint ipsa sit corporis perspiciatis
            officiis inventore, unde. Necessitatibus impedit fuga, quasi error
            numquam atque cumque dicta vero vitae iusto eos dignissimos ducimus
            dolor inventore repudiandae perferendis minus. Architecto illum
            consequatur necessitatibus, dolores, id commodi ipsam asperiores
            culpa maxime alias reprehenderit! Vel sunt animi magnam illum quasi,
            dolore? Amet, veritatis quasi, praesentium nihil perferendis vero,
            est nisi nobis, fugit numquam modi atque voluptatibus harum vitae
            qui a! Ducimus culpa sunt dolor. Reprehenderit exercitationem
            tenetur fugit ab accusamus commodi, deleniti, error cumque.
            Exercitationem, vitae cumque quasi deserunt, maiores incidunt aut,
            quisquam saepe harum error fuga ea! Minima quasi in, voluptatum
            molestiae odio explicabo sequi expedita consectetur tempore quaerat
            aperiam tenetur sed rerum assumenda ut ducimus corporis soluta
            perferendis magni dicta voluptate, repudiandae eos eligendi.
            Sapiente laudantium at architecto incidunt adipisci magnam
            doloremque ullam rerum eos libero tempore eveniet voluptatum alias
            placeat cumque quis consequatur soluta, harum, id atque? Non soluta
            reiciendis beatae, quia incidunt mollitia. Deserunt laboriosam
            necessitatibus minima accusamus natus vero tempora nihil sequi
            libero. In accusamus iste itaque sequi iusto, molestias quo vel
            maxime pariatur, nisi nesciunt quidem reiciendis corporis,
            quibusdam? Animi atque eaque quae voluptatem. lorem Lorem ipsum
            dolor sit amet, consectetur adipisicing elit. Nesciunt culpa
            laboriosam labore temporibus sint ipsa sit corporis perspiciatis
            officiis inventore, unde. Necessitatibus impedit fuga, quasi error
            numquam atque cumque dicta vero vitae iusto eos dignissimos ducimus
            dolor inventore repudiandae perferendis minus. Architecto illum
            consequatur necessitatibus, dolores, id commodi ipsam asperiores
            culpa maxime alias reprehenderit! Vel sunt animi magnam illum quasi,
            dolore? Amet, veritatis quasi, praesentium nihil perferendis vero,
            est nisi nobis, fugit numquam modi atque voluptatibus harum vitae
            qui a! Ducimus culpa sunt dolor. Reprehenderit exercitationem
            tenetur fugit ab accusamus commodi, deleniti, error cumque.
            Exercitationem, vitae cumque quasi deserunt, maiores incidunt aut,
            quisquam saepe harum error fuga ea! Minima quasi in, voluptatum
            molestiae odio explicabo sequi expedita consectetur tempore quaerat
            aperiam tenetur sed rerum assumenda ut ducimus corporis soluta
            perferendis magni dicta voluptate, repudiandae eos eligendi.
            Sapiente laudantium at architecto incidunt adipisci magnam
            doloremque ullam rerum eos libero tempore eveniet voluptatum alias
            placeat cumque quis consequatur soluta, harum, id atque? Non soluta
            reiciendis beatae, quia incidunt mollitia. Deserunt laboriosam
            necessitatibus minima accusamus natus vero tempora nihil sequi
            libero. In accusamus iste itaque sequi iusto, molestias quo vel
            maxime pariatur, nisi nesciunt quidem reiciendis corporis,
            quibusdam? Animi atque eaque quae voluptatem. Lorem ipsum dolor sit
            amet, consectetur adipisicing elit. Nesciunt culpa laboriosam labore
            temporibus sint ipsa sit corporis perspiciatis officiis inventore,
            unde. Necessitatibus impedit fuga, quasi error numquam atque cumque
            dicta vero vitae iusto eos dignissimos ducimus dolor inventore
            repudiandae perferendis minus. Architecto illum consequatur
            necessitatibus, dolores, id commodi ipsam asperiores culpa maxime
            alias reprehenderit! Vel sunt animi magnam illum quasi, dolore?
            Amet, veritatis quasi, praesentium nihil perferendis vero, est nisi
            nobis, fugit numquam modi atque voluptatibus harum vitae qui a!
            Ducimus culpa sunt dolor. Reprehenderit exercitationem tenetur fugit
            ab accusamus commodi, deleniti, error cumque. Exercitationem, vitae
            cumque quasi deserunt, maiores incidunt aut, quisquam saepe harum
            error fuga ea! Minima quasi in, voluptatum molestiae odio explicabo
            sequi expedita consectetur tempore quaerat aperiam tenetur sed rerum
            assumenda ut ducimus corporis soluta perferendis magni dicta
            voluptate, repudiandae eos eligendi. Sapiente laudantium at
            architecto incidunt adipisci magnam doloremque ullam rerum eos
            libero tempore eveniet voluptatum alias placeat cumque quis
            consequatur soluta, harum, id atque? Non soluta reiciendis beatae,
            quia incidunt mollitia. Deserunt laboriosam necessitatibus minima
            accusamus natus vero tempora nihil sequi libero. In accusamus iste
            itaque sequi iusto, molestias quo vel maxime pariatur, nisi nesciunt
            quidem reiciendis corporis, quibusdam? Animi atque eaque quae
            voluptatem. lorem
          </p>
        </Container>
      </React.Fragment>
    );
  }
}

export default About;
