import React, { Component } from "react";
import { Container } from "reactstrap";
import { Link } from "react-router-dom";

class PageNotFound extends Component {
  render() {
    return (
      <Container>
        <hr />
        <h3 className="display-4 text-center">
          Ooops. This page doesn't exist!
          <br />
          <Link className="links" to={"/"}>
            Back to the main page
          </Link>
        </h3>
        <hr />
      </Container>
    );
  }
}

export default PageNotFound;
