export class ApiService {
  static async request(input, init = {}) {
    const res = await fetch(input, {
      ...init,
      headers: {
        'Content-Type': 'application/json',
        ...init.headers,
      }
    });
    return await res.json();
  }

  static get(url, options = {}) {
    return ApiService.request(url, {
      method: 'GET',
      ...options,
    })
  }

  static post(url, body, options = {}) {
    return ApiService.request(url, {
      method: 'POST',
      body: body ? JSON.stringify(body) : undefined,
      ...options,
    })
  }

  static put(url, body, options = {}) {
    return ApiService.request(url, {
      method: 'PUT',
      body: body ? JSON.stringify(body) : undefined,
      ...options,
    })
  }

  static delete(url, body, options = {}) {
    return ApiService.request(url, {
      method: 'DELETE',
      body: body ? JSON.stringify(body) : undefined,
      ...options,
    })
  }
}