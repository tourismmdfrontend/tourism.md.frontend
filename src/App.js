import React, { Component } from "react";
import { Switch, Route } from "react-router-dom";
import "./App.css";
import "bootstrap/dist/css/bootstrap.css";
import PageNotFound from "./components/PageNotFound";
import Navigation from "./components/Navigation";
import Footer from "./components/Footer";
import General from "./components/General";
import Travel from "./components/Travel";
import About from "./components/About";
import ListPointOfInterest from "./components/ListPointOfInterest";
import { PointOfInterestDetail } from "./components/PointOfInterestDetail";
import Sign from "./components/Sign";
import Login from "./components/Login";

class App extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Switch>
          <Route exact path={["/signup", "/login"]} />
          {/* render={() => null}  */}
          <Route component={Navigation} />
        </Switch>
        <Switch>
          <Route path="/signup" component={Sign} />
          <Route path="/login" component={Login} />
          <Route exact path="/" component={General} />
          <Route path="/attractions/:id" component={PointOfInterestDetail} />
          <Route path="/attractions" component={ListPointOfInterest} />
          <Route path="/travel" component={Travel} />
          <Route path="/about" component={About} />
          <Route component={PageNotFound} />
        </Switch>
        <Switch>
          <Route exact path={["/signup", "/login"]} />
          {/* render={() => null}  */}
          <Route component={Footer} />
        </Switch>
      </React.Fragment>
    );
  }
}

export default App;
