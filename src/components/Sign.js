import React, { Component } from "react";
import { Container, Button, Form, FormGroup, Input } from "reactstrap";
import { Parallax } from "react-parallax";
import { Link } from "react-router-dom";

class Sign extends Component {
  render() {
    return (
      <React.Fragment>
        <Parallax
          blur={0}
          bgImage={require("../components/img/sign.png")}
          bgImageAlt="nature"
          bgImageStyle={{ opacity: "0.9" }}
          strength={500}
        >
          <Container className="text-center centered">
            <hr />
            <h2>New on this site?</h2>
            <h5>
              Create an account or{" "}
              <Link className="guest_link" to={"/"}>
                continue as a guest
              </Link>
            </h5>
            <hr />
            <section className="row justify-content-center align-items-center mt-5">
              <section className="col-10 col-xl-4 col-lg-6 col-sm-8">
                <Form className="form-container log">
                  <h3>CREATE ACCOUNT</h3>
                  <FormGroup className="mt-4">
                    <Input
                      type="text"
                      name="text"
                      id="exampleFullName"
                      placeholder="enter your full name..."
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="email"
                      name="email"
                      id="exampleEmail"
                      placeholder="enter your email..."
                    />
                  </FormGroup>
                  <FormGroup>
                    <Input
                      type="password"
                      name="password"
                      id="examplePassword"
                      placeholder="enter your password..."
                    />
                  </FormGroup>

                  <Button color="primary" type="submit" tag={Link} to={"/"}>
                    Register
                  </Button>
                  <br />
                  <small>
                    Have already an account?{" "}
                    <Link className="login_link" to={"/login"}>
                      <strong>Login here</strong>
                    </Link>
                  </small>
                </Form>
              </section>
            </section>
          </Container>

          <div style={{ height: "800px" }} />
        </Parallax>
      </React.Fragment>
    );
  }
}

export default Sign;
