import React, { Component } from "react";
import { Container } from "reactstrap";
import PointOfInterest from "../components/PointOfInterest";
import SomeData from "../data/PostData.json";

class ListPointOfInterest extends Component {
  state = {
    PostData: []
  };

  componentDidMount() {
    setTimeout(() => {
      this.setState({
        PostData: SomeData
      });
    }, 0);
  }

  render() {
    const { PostData } = this.state;
    return (
      <React.Fragment>
        <Container className="text-center">
          <hr />
          <h3 className="display-4">Our Points Of Interest</h3>
          <hr />
          <div className="row">
            {PostData.map((post, index) => (
              <div className="col-4">
                <PointOfInterest postDetail={post} />
              </div>
            ))}
          </div>
        </Container>
      </React.Fragment>
    );
  }
}

export default ListPointOfInterest;

// src={require("../components/img/abc.jpg")}
