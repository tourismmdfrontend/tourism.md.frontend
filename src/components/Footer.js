import React, { Component } from "react";
import { Container } from "reactstrap";

class Footer extends Component {
  state = {};
  render() {
    return (
      <React.Fragment>
        <Container>
          <hr />
          <footer className="page-footer">
            <div className="footer-copyright text-center py-3">
              Copyright &copy; Winify Internship 2019. All Rights Reserved
            </div>
          </footer>
          <hr />
        </Container>
      </React.Fragment>
    );
  }
}

export default Footer;
